{% extends 'base.php' %}

{% block content %}
	<div class="container">
		<section class="row">
			{% if wp.have_posts() %}
				{% do wp.the_post() %}

				{% do wp.get_template_part('content', 'page') %}
			{% endif %}
		</section>
	</div>
{% endblock %}