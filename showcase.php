{# Modal #}
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{% do wp._e('Close', 'brilia') %}</span></button>
				<h4 id="modal-title" class="modal-title">{% do wp._e('Portfolio Showcase', 'brilia') %}</h4>
			</div>
			<div class="modal-body">
				<div class="carousel slide">
					{# Indicators #}
					<ol class="carousel-indicators">
						{% for i in 0..12 %}

							<li data-target=".carousel" data-slide-to="{{ i }}"></li>

						{% endfor %}
					</ol>

					{# Wrapper for slides #}
					<div class="carousel-inner">
						{% for i in 0..12 %}

							<figure class="item" role="img">
								<img alt="" class="aligncenter img-responsive" src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" height="411" width="411" />
							</figure>

						{% endfor %}
					</div>

					{# Controls #}
					<a class="left carousel-control" href=".carousel" role="button" data-slide="prev"><span class="icon-prev"></span><span class="sr-only">{% do wp._e('Prev', 'brilia') %}</span></a>
					<a class="right carousel-control" href=".carousel" role="button" data-slide="next"><span class="icon-next"></span><span class="sr-only">{% do wp._e('Next', 'brilia') %}</span></a>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>