<?php

class Twig_Proxy {

	private $twig = null;

	function __construct($twig) {
		$this->twig = $twig;
	}

	public function __call($func, $args) {
		$return = null;

		if (function_exists($func)) {
			$return = call_user_func_array($func, $args);
		} else {
			trigger_error('call to unexisting function ' . $func, E_USER_ERROR);
		}

		return $return;
	}

	public function get_template_part($slug, $name = null) {
		ob_start();
		get_template_part($slug, $name);
		$this->twig->display(ob_get_clean());
	}

}
