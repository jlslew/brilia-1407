<?php

function display_quotes() {
	static $quotes = [];

	if (empty($quotes)) {
		$quotes = require_once __DIR__ . '/../quotes.php';
		shuffle($quotes);
	}

	$quote = array_shift($quotes);
	return "<p>{$quote['text']}</p><cite>{$quote['source']}</cite>";
}

function display_portfolios() {
	static $portfolios = [];

	if (empty($portfolios)) {
		$portfolios = glob(__DIR__ . '/../portfolios/*');
		shuffle($portfolios);
	}

	return 'background-image:url(' . get_template_directory_uri() . '/portfolios/'
			. basename(array_shift($portfolios)) . ')';
}

/**
 * Display navigation to next/prev post when applicable.
 */
function brilia_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$prev = ( is_attachment() ) ? get_post(get_post()->post_parent) : get_adjacent_post(false, '', true);
	$next = get_adjacent_post(false, '', false);

	if (!$next && !$prev) {
		return;
	}
	?>
	<nav class="navigation" role="navigation">
		<h1 class="sr-only"><?php _e('Post navigation', 'brilia'); ?></h1>
		<ul class="pager">
			<li class="prev"><?php previous_post_link('%link', __('&larr;&nbsp;%title', 'brilia')); ?></li>
			<li class="next"><?php next_post_link('%link', __('%title&nbsp;&rarr;', 'brilia')); ?></li>
		</ul>
	</nav>
	<?php
}

/**
 * Print HTML with meta information for the current post-date/time and author.
 */
function brilia_posted_on() {
	// Set up and print post meta information.
	printf('<a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a> <span class="byline"><span class="author vcard"><a class="url fn n" href="%4$s" rel="author">%5$s</a></span></span>', esc_url(get_permalink()), esc_attr(get_the_date('c')), esc_html(get_the_date()), esc_url(get_author_posts_url(get_the_author_meta('ID'))), get_the_author());
}
