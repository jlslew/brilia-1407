{% spaceless %}
	{% do wp.get_template_part('header') %}
	{% block content %}
		<p>{% do wp._e('Hello World!', 'brilia') %}</p>
	{% endblock %}
	{% do wp.get_template_part('footer') %}
{% endspaceless %}