<article {% do wp.post_class('col-sm-12') %}>
	<header class="entry-header">
		{% do wp.the_title('<h1 class="entry-title">', '</h1>') %}
	</header>
	<div class="entry-content">
		{% do wp.the_content() %}
		{% do wp.wp_link_pages({'next_or_number': 'next'}) %}
	</div>
	<footer class="entry-footer">
		{% do wp.edit_post_link(wp.__('Edit', 'brilia'), '<span class="edit-link">', '</span>') %}
	</footer>
</article>