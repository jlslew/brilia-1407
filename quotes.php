<?php

return [
	[
		'text' => 'The hottest new branding shop in town. Keep an eye on this company!',
		'source' => 'Jodie McNamara, Pluck Events - 1',
	],
	[
		'text' => 'The hottest new branding shop in town. Keep an eye on this company!',
		'source' => 'Jodie McNamara, Pluck Events - 2',
	],
	[
		'text' => 'The hottest new branding shop in town. Keep an eye on this company!',
		'source' => 'Jodie McNamara, Pluck Events - 3',
	],
	[
		'text' => 'The hottest new branding shop in town. Keep an eye on this company!',
		'source' => 'Jodie McNamara, Pluck Events - 4',
	],
];
