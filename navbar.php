<nav class="navbar navbar-fixed" role="navigation">
	<div class="container-fluid">
		{# Brand and toggle get grouped for better mobile display #}
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" data-target=".navbar-collapse" data-toggle="collapse" type="button">
				<span class="sr-only">{% do wp._e('Toggle navigation', 'brilia') %}</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ wp.esc_url(wp.home_url('/#home')) }}" rel="home"><span class="sr-only">{% do wp.bloginfo('name') %}</span></a>
		</div>

		{# Collect the nav links, forms, and other content for toggling #}
		<div class="collapse navbar-collapse">
			<ul class="nav nav-justified navbar-nav">
				<li><a href="{{ wp.esc_url(wp.home_url('/#work')) }}">{% do wp._e('WORK', 'brilia') %}</a></li>
				<li><a href="{{ wp.esc_url(wp.home_url('/#services')) }}">{% do wp._e('SERVICES', 'brilia') %}</a></li>
				<li><a href="{{ wp.esc_url(wp.home_url('/#connect')) }}">{% do wp._e('CONNECT', 'brilia') %}</a></li>
				<li><a href="{{ wp.esc_url(wp.home_url('/#contact')) }}">{% do wp._e('CONTACT', 'brilia') %}</a></li>
			</ul>

			<div class="navbar-right text-center">
				<button class="btn btn-primary navbar-btn" type="button">{% do wp._e('GET QUOTE', 'brilia') %}</button>
				<button class="btn btn-inverse navbar-btn" type="button">{% do wp._e('JOIN BRILIA', 'brilia') %}</button>
			</div>
		</div>
	</div>
</nav>