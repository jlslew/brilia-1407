{% extends 'base.php' %}

{% block content %}
	<div id="home" class="section">
		<header class="section-header">
			<h1 class="section-title">{% do wp._e('Brilliantly Creative.', 'brilia') %}</h1>
			<div class="section-meta">
				<p>{% do wp._e('Through the art of storytelling and a passion for ground-breaking design, we create compelling brands with stories that resonate with their audience.', 'brilia') %}</p>
				<p>{% do wp._e('Use the scroll bar, the rolly ball on your mouse, or the menu at the top of our page to discover the resplendent quality of our work.', 'brilia') %}</p>
			</div>
		</header>
		<footer class="section-footer">
			<blockquote>{{ wp.display_quotes() | raw }}</blockquote>
		</footer>
	</div>

	<figure class="section-transition section-transition-design" role="img"></figure>

	<div id="work" class="section">
		<header class="section-header section-header-design">
			<h1 class="section-title">{% do wp._e('Our Work. Our Passion.', 'brilia') %}</h1>
			<div class="section-meta">
				<p>{% do wp._e('We put our heart and soul into conceiving the best creative solutions for every client. By tapping into new and old experiences, we bring a playfulness to our creative process and we may even find a reason to use a Star Wars reference or two ;)', 'brilia') %}</p>
				<p>{% do wp._e('Check out these portfolio pieces to see what we have done for our clients.', 'brilia') %}</p>
			</div>
		</header>
		<div class="section-body container">
			<section class="row masonry">
				<div class="col-sm-12 col-md-6 portfolio wide" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio item" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio tall" data-sr style="{{ wp.display_portfolios() }}"></div>

				<div class="col-sm-6 col-md-3 portfolio" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio tall" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio" data-sr style="{{ wp.display_portfolios() }}"></div>

				<div class="col-sm-6 col-md-3 portfolio" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio" data-sr style="{{ wp.display_portfolios() }}"></div>

				<div class="col-sm-12 col-md-6 portfolio more" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio more" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-6 col-md-3 portfolio more" data-sr style="{{ wp.display_portfolios() }}"></div>
				<div class="col-sm-12 col-md-6 portfolio wide more" data-sr style="{{ wp.display_portfolios() }}"></div>
			</section>
			<button id="btn-more" class="btn btn-link" type="button">{% do wp._e('MORE PORTFOLIOS', 'brilia') %}</button>
		</div>
		<footer class="section-footer">
			<blockquote>{{ wp.display_quotes() | raw }}</blockquote>
		</footer>
	</div>

	<figure class="section-transition section-transition-inspire" role="img"></figure>

	<div id="services" class="section">
		<header class="section-header section-header-inspire">
			<h1 class="section-title">{% do wp._e('Everything We Do.', 'brilia') %}</h1>
			<div class="section-meta">
				<p>{% do wp._e('From design to marketing, we aspire to inspire and to be inspired.', 'brilia') %}</p>
				<p>{% do wp._e('Check out these portfolio pieces to see what we have done for our clients.', 'brilia') %}</p>
			</div>
		</header>
		<div class="section-body container">
			<section class="row">
				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-brand-development"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('BRAND DEVELOPMENT', 'brilia') %}</h2>
						<p>{% do wp._e('Whether it&#39;s a fresh start or a rebrand, Brilia can make a unique statement.', 'brilia') %}</p>
					</figcaption>
				</figure>
				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-marketing"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('MARKETING', 'brilia') %}</h2>
						<p>{% do wp._e('Focus your market value by targeting consumers with innovative ideas.', 'brilia') %}</p>
					</figcaption>
				</figure>
				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-web-development"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('WEB DEVELOPMENT', 'brilia') %}</h2>
						<p>{% do wp._e('The websites we produce will embody the interactive, the professional, and the unique all in one.', 'brilia') %}</p>
					</figcaption>
				</figure>

				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-graphic-design"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('GRAPHIC DESIGN', 'brilia') %}</h2>
						<p>{% do wp._e('From logos to charts - we ensure your brand can compete in today&#39;s market.', 'brilia') %}</p>
					</figcaption>
				</figure>
				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-exhibit-design"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('EXHIBIT DESIGN', 'brilia') %}</h2>
						<p>{% do wp._e('We provide the tools for trade shows to make your brand stand out.', 'brilia') %}</p>
					</figcaption>
				</figure>
				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-market-research"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('MARKET RESEARCH', 'brilia') %}</h2>
						<p>{% do wp._e('We focus your brand to better suit your target demographic.', 'brilia') %}</p>
					</figcaption>
				</figure>

				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-printing"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('PRINTING', 'brilia') %}</h2>
						<p>{% do wp._e('High quality printing services for any type of private or commercial project.', 'brilia') %}</p>
					</figcaption>
				</figure>
				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-social-media"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('SOCIAL MEDIA', 'brilia') %}</h2>
						<p>{% do wp._e('We believe in social media that captivates your followers every step of the way.', 'brilia') %}</p>
					</figcaption>
				</figure>
				<figure class="col-sm-12 col-md-4 service" role="img">
					<div class="icon icon-writing-editing"></div>
					<figcaption class="caption">
						<h2 class="title">{% do wp._e('WRITING AND EDITING', 'brilia') %}</h2>
						<p>{% do wp._e('Creative or technical, marketing campaigns to annual reports, we do it all!', 'brilia') %}</p>
					</figcaption>
				</figure>
			</section>
		</div>
	</div>

	<figure class="section-transition section-transition-explore" role="img"></figure>

	<div id="connect" class="section">
		<header class="section-header section-header-explore">
			<h1 class="section-title">{% do wp._e('Everything We Do.', 'brilia') %}</h1>
			<div class="section-meta">
				<p>{% do wp._e('From design to marketing, we aspire to inspire and to be inspired.', 'brilia') %}</p>
				<p>{% do wp._e('Check out these portfolio pieces to see what we have done for our clients.', 'brilia') %}</p>
			</div>
		</header>
		<div class="section-body container">
			<section class="row">
				<div class="col-sm-12 col-md-6">
					<ul class="entry">
						{% for i in 0..2 %}
							{% if wp.have_posts() %}
								<li class="entry-item">
									{% do wp.the_post() %}

									<h2 class="entry-item-heading">{% do wp.the_title('<a href="' ~ wp.esc_url(wp.get_permalink()) ~ '" rel="bookmark">', '</a>') %}</h2>
									{% do wp.the_post_thumbnail() %}
									<div class="entry-item-text">{% do wp.the_excerpt() %}</div>
								</li>
							{% endif %}
						{% endfor %}
					</ul>
				</div>
				<div class="col-sm-12 col-md-6">
					<h2 class="twitter-heading">{% do wp._e('TWEETS', 'brilia') %}</h2>
					<div class="twitter-wrapper">
						<a class="twitter-timeline" data-border-color="#ffffff" data-chrome="noheader noscrollbar transparent" data-link-color="#f9d800" data-widget-id="439642060472070146" href="//twitter.com/BriliaCreative">Tweets by @BriliaCreative</a>
					</div>
				</div>
			</section>
		</div>
		<footer class="section-footer">
			<blockquote>{{ wp.display_quotes() | raw }}</blockquote>
		</footer>
	</div>

	<figure class="section-transition section-transition-engage" role="img"></figure>

	<div id="contact" class="section">
		<header class="section-header section-header-engage">
			<h1 class="section-title">{% do wp._e('Get in Touch', 'brilia') %}</h1>
			<div class="section-meta">
				<p>{% do wp._e('We&#39;re very approachable and would love to speak to you. Feel free to call, send us an email, Tweet us or simply complete the enquiry form.', 'brilia') %}</p>
			</div>
		</header>
		<div class="section-body container">
			<section class="row">
				<div class="col-sm-12 col-md-6">
					<a class="method method-phone" href="tel:6138662177">613 866 2177</a>
					<a class="method method-email" href="{{ wp.antispambot('mailto:hello@briliacreative.com') | raw }}">{{ wp.antispambot('hello.briliacreative.com') | raw }}</a>
					<a class="method method-facebook" href="//facebook.com/BriliaCreative">https://facebook.com/BriliaCreative</a>
					<a class="method method-twitter" href="//twitter.com/BriliaCreative">https://twitter.com/BriliaCreative</a>
					<a class="method method-tumblr" href="//briliacreative.tumblr.com">http://briliacreative.tumblr.com</a>
				</div>
				<div class="col-sm-12 col-md-6">
					<h2 class="form-heading">{% do wp._e('REQUEST A QUOTE', 'brilia') %}</h2>
					<div class="form-wrapper">{{ wp.do_shortcode('[contact-form-7 id="1705" title="Request-A-Quote-Form"]') | raw }}</div>
				</div>
			</section>
		</div>
	</div>
	{% do wp.get_template_part('showcase') %}
{% endblock %}