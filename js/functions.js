+function($) {
    'use strict';

    var siteurl = $('body').data('siteurl');

    $.getScript('//use.typekit.net/smd8dws.js', function() {
        Typekit.load({
            inactive: function() {
                $('head').append('<link rel="stylesheet" href="' + siteurl + '/futura-pt.css" media="all" />');
            }
        });
    });

    $(function() {
        $('#splash').delay(3000).fadeOut('slow');
    });

    $('.navbar a').click(function() {
        $('html, body').animate({
            scrollTop: $(this.hash).offset().top
        }, 1200);
    });

    $('.section-title').css('fontSize', function() {
        var height = 219 / 2498 * $(window).width();

        if (height > 75)
            $(this).parent().css('paddingTop', height - 55);

        return Math.min(height * .8, 60);
    });


    /**
     *  Home Section
     */

    $(function() {
        $('#home .section-header').css('height', $(window).height() - $('.navbar').height());
    });


    /**
     *  Work Section
     */

    (function($this) {
        (function($item) {
            $('.carousel-indicators').children().first().addClass('active');
            $('.carousel-inner').children().first().addClass('active');

            $item.css('height', function() {
                if ($(this).hasClass('tall'))
                    return (parseInt($(this).css('width')) * 2) - 1;

                if ($(this).hasClass('wide'))
                    return (parseInt($(this).css('width')) - 1) / 2;

                return $(this).css('width');
            }).click(function(e) {
                e.preventDefault();

                $('.carousel').carousel($(this).index());
                $('.modal').modal('show');
            });

            $item.each(function(i) {
                var img = $(this).css('backgroundImage').replace(/url\(|\)|"|'/g, '');
                $('.carousel-inner').children().eq(i).find('img').attr('src', img);
            });
        })($this.find('.portfolio'));

        $this.masonry({
            itemSelector: '.portfolio',
            columnWidth: '.item'
        });

        $this.next('#btn-more').click(function() {
            $this.find('.more').toggleClass('hide');
            $this.masonry('layout');
        }).click();

        window.sr = new scrollReveal({
            vFactor: 0.33,
            reset: true
        });
    })($('#work .masonry'));


    /**
     *  Connect Section
     */

    $.getScript('//platform.twitter.com/widgets.js', function() {
        twttr.events.bind('rendered', function() {
            $('.twitter-timeline').contents().find('head link').attr('href', siteurl + '/twitter.css');
        });
    });

    /**
     *  Contact Section
     */

    $('#contact .form-control').each(function() {
        var $this = $(this);

        $this.data('text', $this.val().replace(/<br>/g, '\n'));
        $this.val($this.data('text'));

        $this.focus(function() {
            if ($this.data('text') === $this.val())
                $this.val('');
        }).blur(function() {
            if ($this.val() === '')
                $this.val($this.data('text'));
        });
    });
}(jQuery);