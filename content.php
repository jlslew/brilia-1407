<article {% do wp.post_class('col-sm-12') %}>
	<header class="entry-header">
		{% do wp.the_title(wp.sprintf('<h1 class="entry-title"><a href="%s" rel="bookmark">', wp.esc_url(wp.get_permalink())), '</a></h1>') %}

		{% if 'post' == wp.get_post_type() %}
			<div class="entry-meta">
				{% do wp.brilia_posted_on() %}
			</div>
		{% endif %}
	</header>
	<div class="entry-content">
		{% do wp.the_content(wp.__('Continue reading <span class="meta-nav">&rarr;</span>', 'brilia')) %}
		{% do wp.wp_link_pages({'next_or_number': 'next'}) %}
	</div>
	<footer class="entry-footer">
		{% do wp.edit_post_link(wp.__('Edit', 'brilia'), '<span class="edit-link">', '</span>') %}
	</footer>
</article>