<!DOCTYPE html>
<html {% do wp.language_attributes() %}>
	<head>
		<meta charset="{% do wp.bloginfo('charset') %}" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>{% do wp.wp_title('|', true, 'right') %}</title>
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="{% do wp.bloginfo('pingback_url') %}" />
		<style>[data-sr]{visibility:hidden;}</style>
		{% do wp.wp_head() %}
	</head>
	<body {% do wp.body_class() %} data-siteurl="{{ wp.get_template_directory_uri() }}">
		<header id="splash" role="banner"></header>

		{% do wp.get_template_part('navbar') %}